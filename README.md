# Latar Belakang
Internet of Things (IoT) adalah konsep yang menghubungkan perangkat fisik ke internet. Perangkat-perangkat ini dapat dikendalikan dan dimonitor dari jarak jauh, sehingga memberikan kemudahan dan efisiensi bagi penggunanya. Salah satu penerapan IoT yang populer adalah sistem kendali lampu. Sistem ini memungkinkan pengguna untuk menyalakan dan mematikan lampu dari jarak jauh, sehingga dapat menghemat energi dan meningkatkan keamanan. Proyek IoT lampu yang bisa menyala sendiri pada jam tertentu dan dapat di kontrol dari jarak jauh ini bertujuan untuk membuat sistem kendali lampu yang lebih fleksibel dan efisien. Sistem ini menggunakan sensor waktu untuk menyalakan lampu secara otomatis pada jam tertentu, dan menggunakan aplikasi smartphone untuk mengendalikan lampu dari jarak jauh.

# Branding
- Merk : LumiSmart Pro
- Inspirasi Merk : "LumiSmart Pro" adalah nama yang mencerminkan bahwa produk ini adalah lampu cerdas yang sangat canggih, mampu memberikan pencahayaan pintar, dan sangat cocok untuk kebutuhan pengguna yang ingin mengontrol pencahayaan dari jarak jauh dan secara cerdas.
- Tagline : Praktis. Pecahkan Kegelapan.
- Campaign : Bagaimana menjadikan rumah / kostan / kontrakan kemudahan dan kenyamanan saat berada di luar rumah.
- Target User : orang yang sering berpergian dan jauh dari rumah
- User experience theme:
    - Mudah dikonfigurasi dan digunakan
    - Terknoneksi dengan aplikasi berbasis mobile
 
# User Story 
| sebagai | saya ingin bisa | sehingga | prioritas |
|---|---|---|---|
| system | membaca waktu berdasarkan timezone internet | Bisa memilih respon yang sesuai | tinggi |
| system smarthome | dapat membaca Zona waktu internet | Bisa menjadi input kalkulasi respon yang sesuai | tinggi |
| system smarthome | dapat terintegrasi dengan lampu | Bisa menjadi kalkulasi respond yang lebih akurat | menengah |
| system | memberikan respond terhadap keadaan | pengguna dapat lebih terbantu dalam berbagai aspek | tinggi |

# Metode dan Algoritma
- Metode 
    Metode yang digunakan adalah Network Time Protocol (NTP). NTP adalah protokol yang digunakan untuk sinkronisasi waktu antara dua sistem komputer. NTP menggunakan server waktu yang tersinkronisasi dengan waktu UTC untuk mengirimkan waktu ke perangkat klien.
- Algoritma
    - IF else sederhana
    - perangkat klien mengirimkan permintaan waktu ke server waktu.
    - Server waktu menanggapi permintaan dengan waktu saat ini.
    - Perangkat klien menggunakan waktu yang diterima dari server waktu untuk sinkronisasi waktunya sendiri.
- Blynk App
# Struktur Data 
![Struktur Data](Untitled_Diagram.drawio_3_.png).
# Arsitektur Sistem
[![](https://mermaid.ink/img/pako:eNpFT7mOwjAQ_ZXR1KTZ7VIgkQMokVhpC5tiiCfESnzIcRStEP--NovY6s075rpj5xRjif3k1m6gEOGrkXYnjLvqicEPzvIFimILjWjPJ_j8gEwTr4QP7hbIgOLI46wv0kLztOq3FXj2zqps1X9trfjX2iTsxUTGL4lVT_8gvnWfZx0zraQ9ZDyKlcb4Sm1raesEO9yg4WBIq_TBPe0AiXFgwxLLVCoKo0RpHylHS3TnH9th2dM08wYXryhyoynf-VIfvz0kV7w?type=png)](https://mermaid.live/edit#pako:eNpFT7mOwjAQ_ZXR1KTZ7VIgkQMokVhpC5tiiCfESnzIcRStEP--NovY6s075rpj5xRjif3k1m6gEOGrkXYnjLvqicEPzvIFimILjWjPJ_j8gEwTr4QP7hbIgOLI46wv0kLztOq3FXj2zqps1X9trfjX2iTsxUTGL4lVT_8gvnWfZx0zraQ9ZDyKlcb4Sm1raesEO9yg4WBIq_TBPe0AiXFgwxLLVCoKo0RpHylHS3TnH9th2dM08wYXryhyoynf-VIfvz0kV7w)
# Deskripsi Teknologi
- Blynk App
- Framework : blynk IoT, Blynk IoT ini memiliki support yang sangat baik dan juga library yang complete yang berhubungan dengan IoT
- Respond : Lampu LED
- Mesin Komputasi : 
    - Android : karena Android dirasa paling cocok dan memiliki pasar yang lumayan tinggi di indonesia dan orang menengah.
    - ESP32 digunakan karena untuk melakukan integrasi dengan jaringan yang terpasang dirumah.
- 
# User experience
![Laman](UX.png).
 - pada gambar di atas diketahui bahwa kondisi disini memperlihatkan kondisi lampu saat ini sehingga memudahkan user untuk memantau kondisi lampu pada saat tidak ada di rumah
 - tombol on dan juga off disana digunakan untuk mengubah kondisi pada lampu

#Video
Link penjelasan
<figure class="video_container">
  <iframe src="https://youtu.be/R2MfsjPPJTg?si=7HhZHfYDxl9qTbyH" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->



