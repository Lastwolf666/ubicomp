#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "xxxxxx"
#define BLYNK_TEMPLATE_NAME "XXXXXXX"
#define BLYNK_AUTH_TOKEN "XXXXXXXXXX"
#define BLYNK_FIRMWARE_VERSION "0.1.0"
#include <WiFi.h>
#include <BlynkSimpleEsp32.h>

const char *ssid = "XXXXXXXX";
const char *password = "XXXXXXXXX";

const int ledPin = 2; // Pin untuk LED (contoh)
const int relay = 15; //for relay
BlynkTimer timer;

// Blynk virtual pin write handler
BLYNK_WRITE(V1)
{
  int value = param.asInt(); // Mendapatkan nilai dari tombol di aplikasi Blynk
  digitalWrite(ledPin, value); // Mengendalikan perangkat berdasarkan nilai tombol
  digitalWrite(relay,value);
}

void setup()
{
  Serial.begin(115200);

  Blynk.begin(BLYNK_AUTH_TOKEN, ssid, password);

  timer.setInterval(1000L, sendSensor);

  // Setup pin untuk output (contoh: LED)
  pinMode(ledPin, OUTPUT);
  pinMode(relay,OUTPUT);
  configTime(7 * 3600, 0, "pool.ntp.org");
  Serial.println("Waiting for time");
  while (!time(nullptr))
  {
    delay(1000);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("Time is synchronized");
}

void loop()
{
  time_t now;
  struct tm timeinfo;
  time(&now);
  localtime_r(&now, &timeinfo);

  // Display current time
  Serial.printf("Current time: %02d:%02d:%02d\n", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

  // Check if the current hour is greater than or equal to 16 (4:00 PM)
  if (timeinfo.tm_hour >= 16)
  {
    // Turn on the relay (assuming it's controlling something)
    digitalWrite(relay, LOW);

    // Turn on the LED
    digitalWrite(ledPin, HIGH);
  }
  else
  {
    // Turn off the relay
    digitalWrite(relay, HIGH);

    // Turn off the LED
    digitalWrite(ledPin, LOW);
  }
  Blynk.run();
  timer.run(); // Memproses timer Blynk

  // Kode loop lainnya di sini
}

void sendSensor()
{
  // Kode untuk membaca sensor dan mengirimkan data ke Blynk
}
